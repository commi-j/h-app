package tk.labyrinth.happ.application.domain.moduleparameter;

import lombok.Value;
import lombok.With;

@Value(staticConstructor = "of")
@With
public class ValueModuleParameter implements ModuleParameter {

	ModuleParameterValue value;

	public static ValueModuleParameter empty() {
		return of(ModuleParameterValue.empty(true));
	}
}
