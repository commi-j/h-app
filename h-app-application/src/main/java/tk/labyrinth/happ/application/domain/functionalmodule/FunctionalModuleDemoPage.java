package tk.labyrinth.happ.application.domain.functionalmodule;

import com.vaadin.flow.component.Composite;
import com.vaadin.flow.router.Route;
import io.vavr.collection.List;
import tk.labyrinth.happ.application.domain.module.FunctionalCorrectionModuleView;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameterValue;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameterValueSign;
import tk.labyrinth.happ.application.domain.moduleparameter.RangeModuleParameter;
import tk.labyrinth.happ.application.domain.moduleparameter.ValueModuleParameter;

@Route("functional-module-demo")
public class FunctionalModuleDemoPage extends Composite<FunctionalCorrectionModuleView> {

	{
		getContent().initialize(FunctionalCorrectionModuleView.State.builder()
				.anatomicalElement("DEMO_ANATOMICAL_ELEMENT")
				.externalParameters(List.of(
						ValueModuleParameter.empty(),
						ValueModuleParameter.empty(),
						ValueModuleParameter.empty(),
						ValueModuleParameter.empty(),
						RangeModuleParameter.empty(),
						ValueModuleParameter.empty()))
				.internalParameters(List.of(
						ValueModuleParameter.of(ModuleParameterValue.builder()
								.sign(ModuleParameterValueSign.UNDEFINED)
								.value("12345")
								.build()),
						RangeModuleParameter.of(
								ModuleParameterValue.builder()
										.sign(ModuleParameterValueSign.MINUS)
										.value("0,00123")
										.build(),
								ModuleParameterValue.builder()
										.sign(ModuleParameterValueSign.PLUS)
										.value("1,01230")
										.build()
						),
						ValueModuleParameter.empty()))
				.selectedCellPath(null)
				.build());
	}
}
