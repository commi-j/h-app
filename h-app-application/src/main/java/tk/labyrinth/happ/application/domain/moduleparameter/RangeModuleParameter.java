package tk.labyrinth.happ.application.domain.moduleparameter;

import lombok.Value;
import lombok.With;

@Value(staticConstructor = "of")
@With
public class RangeModuleParameter implements ModuleParameter {

	ModuleParameterValue from;

	ModuleParameterValue to;

	public static RangeModuleParameter empty() {
		return of(ModuleParameterValue.empty(true), ModuleParameterValue.empty(true));
	}
}
