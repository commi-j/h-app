package tk.labyrinth.happ.application.domain.action;

import lombok.Builder;
import lombok.Value;
import lombok.With;

@Builder(toBuilder = true)
@Value
@With
public class SelectCellAction implements Action {

	public enum ValueIndex {
		FROM,
		TO,
		VALUE
	}

	/**
	 * -1 for sign.
	 */
	Integer cellIndex;

	Integer parameterIndex;

	/**
	 * from, to, value
	 */
	ValueIndex valueIndex;
}
