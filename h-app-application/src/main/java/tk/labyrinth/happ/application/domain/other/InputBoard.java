package tk.labyrinth.happ.application.domain.other;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.domain.widget.CellWidget;
import tk.labyrinth.happ.application.vaadin.HtmlTable;
import tk.labyrinth.happ.application.vaadin.HtmlTableRow;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BorderCollapse;

import java.util.function.Consumer;

public class InputBoard {

	public static List<String> createCommaRow(String middleColumnValue) {
		return List.range(0, 21).map(columnIndex -> columnIndex == 11 ? middleColumnValue : ",");
	}

	public static List<String> createFixedRow(String value) {
		return List.range(0, 21).map(columnIndex -> value);
	}

	public static List<String> createMiddleRow() {
		return List.range(0, 21).map(columnIndex -> "" + (10 - columnIndex));
	}

	public static List<String> createNumberRow(int number, String middleColumnPrefix) {
		return List
				.range(0, 10).map(columnIndex -> "-" + number)
				.append(middleColumnPrefix + number)
				.appendAll(List.range(11, 21).map(columnIndex -> "" + number));
	}

	public static HtmlTable render(Properties properties) {
		return new HtmlTable(
				List
						.of(
								createCommaRow("0,1"),
								createNumberRow(9, ",0"),
								createNumberRow(8, ",0"),
								createNumberRow(7, ",0"),
								createNumberRow(6, ",0"),
								createNumberRow(5, ",0"),
								createNumberRow(4, ",0"),
								createNumberRow(3, ",0"),
								createNumberRow(2, ",0"),
								createNumberRow(1, ",0"),
								createFixedRow("00"),
								createMiddleRow(),
								createFixedRow("0"),
								createNumberRow(1, ","),
								createNumberRow(2, ","),
								createNumberRow(3, ","),
								createNumberRow(4, ","),
								createNumberRow(5, ","),
								createNumberRow(6, ","),
								createNumberRow(7, ","),
								createNumberRow(8, ","),
								createNumberRow(9, ","),
								createCommaRow("1,"))
						.map(row -> new HtmlTableRow(row.map(cellValue -> CellWidget.render(CellWidget.Properties.builder()
								.mode(properties.getEnabled() ? CellWidget.Mode.NORMAL : CellWidget.Mode.DISABLED)
								.onClick(() -> properties.getOnInput().accept(cellValue))
								.value(cellValue)
								.build())))))
				.setBorderCollapse(BorderCollapse.COLLAPSE)
				.setBorderSpacing("0px");
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Boolean enabled;

		@NonNull
		Consumer<String> onInput;
	}
}
