package tk.labyrinth.happ.application.domain.moduleparameter;

import com.vaadin.flow.component.Component;
import io.vavr.Tuple;
import io.vavr.Tuple2;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import lombok.val;
import tk.labyrinth.pandora.misc4j.exception.NotImplementedException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import java.util.Objects;
import java.util.function.Consumer;

public class ModuleParameterView {

	public static Tuple2<Integer, String> readSelectedCellPath(
			@CheckForNull List<String> selectedCellPath) {
		Tuple2<Integer, String> result;
		{
			if (selectedCellPath != null) {
				result = Tuple.of(
						switch (selectedCellPath.get(0)) {
							case "from" -> 1;
							case "to" -> 2;
							case "value" -> 0;
							default -> throw new IllegalArgumentException();
						},
						selectedCellPath.get(1));
			} else {
				result = Tuple.of(null, null);
			}
		}
		return result;
	}

	public static Component render(Properties properties) {
		CssHorizontalLayout result = new CssHorizontalLayout();
		{
			result.addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				result.add(properties.getPrefix());
			}
			{
				val selectedCellBreakdown = readSelectedCellPath(properties.getSelectedCellPath());
				//
				if (properties.getParameter() instanceof RangeModuleParameter rangeParameter) {
					{
						result.add(ModuleParameterValueView.render(ModuleParameterValueView.Properties.builder()
								.onCellSelect(path -> properties.getOnCellSelect().accept(List.of("from", path)))
								.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), 1)
										? selectedCellBreakdown._2()
										: null)
								.value(rangeParameter.getFrom())
								.build()));
					}
					{
						result.add(" < P < ");
					}
					{
						result.add(ModuleParameterValueView.render(ModuleParameterValueView.Properties.builder()
								.onCellSelect(path -> properties.getOnCellSelect().accept(List.of("to", path)))
								.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), 2)
										? selectedCellBreakdown._2()
										: null)
								.value(rangeParameter.getTo())
								.build()));
					}
				} else if (properties.getParameter() instanceof ValueModuleParameter valueParameter) {
					{
						result.add(ModuleParameterValueView.render(ModuleParameterValueView.Properties.builder()
								.onCellSelect(path -> properties.getOnCellSelect().accept(List.of("value", path)))
								.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), 0)
										? selectedCellBreakdown._2()
										: null)
								.value(valueParameter.getValue())
								.build()));
					}
				} else {
					throw new NotImplementedException();
				}
			}
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Consumer<List<String>> onCellSelect;

		@NonNull
		ModuleParameter parameter;

		@NonNull
		String prefix;

		@CheckForNull
		List<String> selectedCellPath;
	}
}
