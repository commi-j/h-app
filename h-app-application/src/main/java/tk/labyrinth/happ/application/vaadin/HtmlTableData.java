package tk.labyrinth.happ.application.vaadin;

import com.vaadin.flow.component.ClickNotifier;
import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;

@Tag("td")
public class HtmlTableData extends HtmlContainer implements ClickNotifier<HtmlTableData> {
	// empty
}
