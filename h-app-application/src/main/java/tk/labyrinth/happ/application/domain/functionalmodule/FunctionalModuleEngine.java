package tk.labyrinth.happ.application.domain.functionalmodule;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameter;

public class FunctionalModuleEngine {

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		@NonNull
		Object anatomicalElement;

		@NonNull
		List<ModuleParameter> externalParameters;

		@NonNull
		List<ModuleParameter> internalParameters;

		@NonNull
		Integer upperExternalParameterCount;
	}
}
