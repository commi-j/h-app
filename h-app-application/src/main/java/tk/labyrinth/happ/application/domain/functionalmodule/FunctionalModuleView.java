package tk.labyrinth.happ.application.domain.functionalmodule;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.UI;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.icon.VaadinIcon;
import com.vaadin.flow.component.progressbar.ProgressBar;
import io.vavr.Tuple;
import io.vavr.Tuple3;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.Value;
import lombok.With;
import lombok.val;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameter;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameterView;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.ui.component.value.box.simple.Label;
import tk.labyrinth.pandora.ui.component.view.RenderableView;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import java.util.Objects;
import java.util.function.Consumer;

@RequiredArgsConstructor
public class FunctionalModuleView extends CssVerticalLayout implements RenderableView<FunctionalModuleView.Properties> {

	private final Label anatomicalElementLabel = new Label();

	private final CssVerticalLayout internalParametersLayout = new CssVerticalLayout();

	private final CssVerticalLayout lowerExternalParametersLayout = new CssVerticalLayout();

	private final CssVerticalLayout upperExternalParametersLayout = new CssVerticalLayout();

	{
		{
			addClassNames(PandoraStyles.LAYOUT);
		}
		{
			{
				add(new Button(
						VaadinIcon.PRINT.create(),
						event -> UI.getCurrent().getPage().executeJs("print();")));
			}
			{
				add("External Parameters");
				upperExternalParametersLayout.addClassNames(PandoraStyles.LAYOUT);
				add(upperExternalParametersLayout);
			}
			{
				ProgressBar progressBar = new ProgressBar();
				progressBar.setHeight("0.1em");
				progressBar.setValue(1);
				add(progressBar);
			}
			{
				add(anatomicalElementLabel.asVaadinComponent());
			}
			{
				add("Internal Parameters");
				internalParametersLayout.addClassNames(PandoraStyles.LAYOUT);
				add(internalParametersLayout);
			}
			{
				ProgressBar progressBar = new ProgressBar();
				progressBar.setHeight("0.1em");
				progressBar.setValue(1);
				add(progressBar);
			}
			{
				add("External Parameters");
				lowerExternalParametersLayout.addClassNames(PandoraStyles.LAYOUT);
				add(lowerExternalParametersLayout);
			}
		}
	}

	@Override
	public Component asVaadinComponent() {
		return this;
	}

	@Override
	public void render(Properties properties) {
		val selectedCellBreakdown = readSelectedCellPath(properties.getSelectedCellPath());
		{
			anatomicalElementLabel.render(Label.Properties.builder()
					.text(properties.getAnatomicalElement().toString())
					.build());
		}
		{
			internalParametersLayout.removeAll();
			properties.getInternalParameters()
					.zipWithIndex()
					.forEach(pair -> internalParametersLayout.add(ModuleParameterView.render(
							ModuleParameterView.Properties.builder()
									.onCellSelect(path -> properties.getOnCellSelect().accept(
											path.prependAll(List.of("internalParameters", pair._2().toString()))))
									.parameter(pair._1())
									.prefix("%s)".formatted(pair._2() + 1))
									.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), true) &&
											Objects.equals(selectedCellBreakdown._2(), pair._2())
											? selectedCellBreakdown._3()
											: null)
									.build())));
		}
		{
			lowerExternalParametersLayout.removeAll();
			properties.getExternalParameters()
					.zipWithIndex()
					.subSequence(properties.getUpperExternalParameterCount())
					.forEach(pair -> lowerExternalParametersLayout.add(ModuleParameterView.render(
							ModuleParameterView.Properties.builder()
									.onCellSelect(path -> properties.getOnCellSelect().accept(
											path.prependAll(List.of("externalParameters", pair._2().toString()))))
									.parameter(pair._1())
									.prefix("%s)".formatted(pair._2() + 1))
									.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), false) &&
											Objects.equals(selectedCellBreakdown._2(), pair._2())
											? selectedCellBreakdown._3()
											: null)
									.build())));
		}
		{
			upperExternalParametersLayout.removeAll();
			properties.getExternalParameters()
					.zipWithIndex()
					.subSequence(0, properties.getUpperExternalParameterCount())
					.forEach(pair -> upperExternalParametersLayout.add(ModuleParameterView.render(
							ModuleParameterView.Properties.builder()
									.onCellSelect(path -> properties.getOnCellSelect().accept(
											path.prependAll(List.of("externalParameters", pair._2().toString()))))
									.parameter(pair._1())
									.prefix("%s)".formatted(pair._2() + 1))
									.selectedCellPath(Objects.equals(selectedCellBreakdown._1(), false) &&
											Objects.equals(selectedCellBreakdown._2(), pair._2())
											? selectedCellBreakdown._3()
											: null)
									.build())));
		}
	}

	public static Tuple3<Boolean, Integer, List<String>> readSelectedCellPath(
			@CheckForNull List<String> selectedCellPath) {
		Tuple3<Boolean, Integer, List<String>> result;
		{
			if (selectedCellPath != null) {
				result = Tuple.of(
						switch (selectedCellPath.get(0)) {
							case "internalParameters" -> true;
							case "externalParameters" -> false;
							default -> throw new IllegalArgumentException();
						},
						Integer.parseInt(selectedCellPath.get(1)),
						selectedCellPath.subSequence(2));
			} else {
				result = Tuple.of(null, null, null);
			}
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Object anatomicalElement;

		@NonNull
		List<ModuleParameter> externalParameters;

		@NonNull
		List<ModuleParameter> internalParameters;

		@NonNull
		Consumer<List<String>> onCellSelect;

		@CheckForNull
		List<String> selectedCellPath;

		@NonNull
		Integer upperExternalParameterCount;
	}
}
