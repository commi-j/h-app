package tk.labyrinth.happ.application.vaadin;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;

@Tag("table")
public class HtmlTable extends HtmlContainer implements CssTable<HtmlTable> {

	public HtmlTable() {
		// no-op
	}

	public HtmlTable(HtmlTableRow... rows) {
		super(rows);
	}

	public HtmlTable(Iterable<HtmlTableRow> rows) {
		super(StreamUtils.from(rows).toArray(HtmlTableRow[]::new));
	}
}
