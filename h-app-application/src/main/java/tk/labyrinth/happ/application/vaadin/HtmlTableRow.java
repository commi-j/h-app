package tk.labyrinth.happ.application.vaadin;

import com.vaadin.flow.component.HtmlContainer;
import com.vaadin.flow.component.Tag;
import tk.labyrinth.pandora.misc4j.java.collectoin.StreamUtils;

@Tag("tr")
public class HtmlTableRow extends HtmlContainer {

	public HtmlTableRow() {
		// no-op
	}

	public HtmlTableRow(HtmlTableData... datas) {
		super(datas);
	}

	public HtmlTableRow(Iterable<HtmlTableData> datas) {
		super(StreamUtils.from(datas).toArray(HtmlTableData[]::new));
	}
}
