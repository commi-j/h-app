package tk.labyrinth.happ.application;

import com.vaadin.flow.component.dependency.CssImport;
import com.vaadin.flow.component.page.AppShellConfigurator;
import com.vaadin.flow.component.page.Push;
import com.vaadin.flow.server.AppShellSettings;
import org.springframework.stereotype.Service;

@CssImport("./style/pandora.css")
@CssImport("./style/pandora-lumo.css")
@CssImport("./style/pandora-colours.css")
@Push
@Service
public class HAppApplicationAppShellConfigurator implements AppShellConfigurator {

	@Override
	public void configurePage(AppShellSettings settings) {
		// no-op
	}
}
