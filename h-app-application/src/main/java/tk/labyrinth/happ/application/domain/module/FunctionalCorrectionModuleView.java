package tk.labyrinth.happ.application.domain.module;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.progressbar.ProgressBar;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.domain.correctionmoduleparametercell.CorrectionModuleParameterCellEditor;
import tk.labyrinth.happ.application.domain.functionalmodule.FunctionalModuleView;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameter;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameterValue;
import tk.labyrinth.happ.application.domain.moduleparameter.ModuleParameterValueSign;
import tk.labyrinth.happ.application.domain.moduleparameter.RangeModuleParameter;
import tk.labyrinth.happ.application.domain.moduleparameter.ValueModuleParameter;
import tk.labyrinth.happ.application.domain.other.InputBoard;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssFlexItem;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssVerticalLayout;
import tk.labyrinth.pandora.reactive.observable.Observable;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import javax.annotation.CheckForNull;
import java.util.Objects;

public class FunctionalCorrectionModuleView extends SplitLayout {

	private final FunctionalModuleView functionalModuleView = new FunctionalModuleView();

	private final CssVerticalLayout primaryLayout = new CssVerticalLayout();
//	private final SplitLayout primaryLayout = new SplitLayout();

	private final SplitLayout secondaryLayout = new SplitLayout();

	private final Observable<State> stateObservable = Observable.notInitialized();

	{
		{
			setSizeFull();
			setSplitterPosition(50);
		}
		{
//			primaryLayout.setOrientation(Orientation.VERTICAL);
//			primaryLayout.setSplitterPosition(90);
			primaryLayout.addClassNames(PandoraStyles.LAYOUT);
			addToPrimary(primaryLayout);
		}
		{
			secondaryLayout.setOrientation(Orientation.VERTICAL);
			secondaryLayout.setSplitterPosition(20);
			addToSecondary(secondaryLayout);
		}
		{
			stateObservable.subscribe(nextState -> {
				{
					primaryLayout.removeAll();
					{
						functionalModuleView.render(FunctionalModuleView.Properties.builder()
								.anatomicalElement(nextState.getAnatomicalElement())
								.externalParameters(nextState.getExternalParameters())
								.internalParameters(nextState.getInternalParameters())
								.onCellSelect(path -> stateObservable.set(nextState.withSelectedCellPath(path)))
								.selectedCellPath(nextState.getSelectedCellPath())
								.upperExternalParameterCount(3)
								.build());
						CssFlexItem.setFlexGrow(primaryLayout, 1);
						primaryLayout.add(functionalModuleView);
					}
					{
						ProgressBar progressBar = new ProgressBar();
						progressBar.setHeight("0.1em");
						progressBar.setValue(1);
						primaryLayout.add(progressBar);
					}
					{
						primaryLayout.add(CorrectionModuleParameterCellEditor.render(
								CorrectionModuleParameterCellEditor.Properties.builder()
										.controlEnabled(false)
										.numberEnabled(nextState.getSelectedCellPath() != null &&
												!Objects.equals(nextState.getSelectedCellPath().last(), "sign"))
										.onInput(input -> onInput(nextState.getSelectedCellPath(), input))
										.signEnabled(nextState.getSelectedCellPath() != null)
										.build()));
					}
				}
				{
					secondaryLayout.removeAll();
					{
						secondaryLayout.addToPrimary(new Label("S0"));
					}
					{
						secondaryLayout.addToSecondary(InputBoard.render(InputBoard.Properties.builder()
								.enabled(nextState.getSelectedCellPath() != null &&
										!Objects.equals(nextState.getSelectedCellPath().last(), "sign"))
								.onInput(input -> onInputBoardInput(nextState.getSelectedCellPath(), input))
								.build()));
					}
				}
			});
		}
	}

	private void onInput(@NonNull List<String> selectedCellPath, List<String> input) {
		boolean signOverNumber = switch (input.get(0)) {
			case "sign" -> true;
			case "number" -> false;
			default -> throw new IllegalArgumentException();
		};
		String inputValue = input.get(1);
		//
		boolean internalOverExternal = switch (selectedCellPath.get(0)) {
			case "internalParameters" -> true;
			case "externalParameters" -> false;
			default -> throw new IllegalArgumentException();
		};
		int parameterIndex = Integer.parseInt(selectedCellPath.get(1));
		String valueIndex = selectedCellPath.get(2);
		String cellIndex = signOverNumber ? "sign" : selectedCellPath.get(3);
		//
		stateObservable.update(currentState -> internalOverExternal
				? currentState.withInternalParameters(currentState.getInternalParameters().update(
				parameterIndex,
				currentParameter -> updateParameter(currentParameter, valueIndex, cellIndex, inputValue)))
				: currentState.withExternalParameters(currentState.getExternalParameters().update(
				parameterIndex,
				currentParameter -> updateParameter(currentParameter, valueIndex, cellIndex, inputValue))));
	}

	private void onInputBoardInput(@NonNull List<String> selectedCellPath, String input) {
		boolean internalOverExternal = switch (selectedCellPath.get(0)) {
			case "internalParameters" -> true;
			case "externalParameters" -> false;
			default -> throw new IllegalArgumentException();
		};
		int parameterIndex = Integer.parseInt(selectedCellPath.get(1));
		String valueIndex = selectedCellPath.get(2);
		String cellIndex = selectedCellPath.get(3);
		//
		stateObservable.update(currentState -> {
			return internalOverExternal
					? currentState.withInternalParameters(currentState.getInternalParameters().update(
					parameterIndex,
					currentParameter -> updateParameter(currentParameter, valueIndex, cellIndex, input)))
					: currentState.withExternalParameters(currentState.getExternalParameters().update(
					parameterIndex,
					currentParameter -> updateParameter(currentParameter, valueIndex, cellIndex, input)));
		});
	}

	public void initialize(State state) {
		stateObservable.set(state);
	}

	public static ModuleParameterValue updateCell(
			ModuleParameterValue currentParameterValue,
			String cellIndex,
			Character cellValue) {
		ModuleParameterValue result;
		{
			if (Objects.equals(cellIndex, "sign")) {
				if (currentParameterValue.getSign() == ModuleParameterValueSign.NOT_APPLICABLE) {
					throw new IllegalArgumentException();
				}
				result = currentParameterValue.withSign(ModuleParameterValueSign.parse(cellValue));
			} else {
				int numericalIndex = Integer.parseInt(cellIndex);
				result = currentParameterValue.withValue("%s%s%s".formatted(
						currentParameterValue.getValue().substring(0, numericalIndex),
						cellValue,
						currentParameterValue.getValue().substring(numericalIndex + 1)));
			}
		}
		return result;
	}

	public static ModuleParameter updateParameter(
			ModuleParameter currentParameter,
			String selectedValueIndex, // 'value' or 'from'/'to'
			String selectedCellIndex, // 'sign' or number
			String input) {
		ModuleParameter result;
		{
			if (currentParameter instanceof RangeModuleParameter rangeModuleParameter) {
				result = switch (selectedValueIndex) {
					case "from" -> rangeModuleParameter.withFrom(
							updateValue(rangeModuleParameter.getFrom(), selectedCellIndex, input));
					case "to" -> rangeModuleParameter.withTo(
							updateValue(rangeModuleParameter.getTo(), selectedCellIndex, input));
					default -> throw new IllegalArgumentException();
				};
			} else if (currentParameter instanceof ValueModuleParameter valueModuleParameter) {
				if (!Objects.equals(selectedValueIndex, "value")) {
					throw new IllegalArgumentException();
				}
				result = ValueModuleParameter.of(
						updateValue(valueModuleParameter.getValue(), selectedCellIndex, input));
			} else {
				throw new IllegalArgumentException();
			}
		}
		return result;
	}

	public static ModuleParameter updateParameterOld(
			ModuleParameter currentParameter,
			String valueSegment,
			String cellIndex,
			Character cellValue) {
		ModuleParameter result;
		{
			if (currentParameter instanceof RangeModuleParameter rangeModuleParameter) {
				result = switch (valueSegment) {
					case "from" -> rangeModuleParameter.withFrom(
							updateCell(rangeModuleParameter.getFrom(), cellIndex, cellValue));
					case "to" -> rangeModuleParameter.withTo(
							updateCell(rangeModuleParameter.getTo(), cellIndex, cellValue));
					default -> throw new IllegalArgumentException();
				};
			} else if (currentParameter instanceof ValueModuleParameter valueModuleParameter) {
				if (!Objects.equals(valueSegment, "value")) {
					throw new IllegalArgumentException();
				}
				result = ValueModuleParameter.of(updateCell(valueModuleParameter.getValue(), cellIndex, cellValue));
			} else {
				throw new IllegalArgumentException();
			}
		}
		return result;
	}

	public static ModuleParameterValue updateValue(
			ModuleParameterValue currentValue,
			String selectedCellIndex, // 'sign' or number
			String input) {
		ModuleParameterValue result;
		{
			if (Objects.equals(selectedCellIndex, "sign")) {
				if (currentValue.getSign() == ModuleParameterValueSign.NOT_APPLICABLE) {
					throw new IllegalArgumentException();
				}
				if (input.length() > 1) {
					throw new IllegalArgumentException();
				}
				result = currentValue.withSign(ModuleParameterValueSign.parse(input.charAt(0)));
			} else {
				int numberIndex = Integer.parseInt(selectedCellIndex);
				//
				boolean isEmpty = currentValue.getValue().charAt(numberIndex) == ' ';
				boolean isLast = currentValue.getValue().length() == numberIndex + 1;
				//
				if (isLast && isEmpty) {
					result = currentValue.withValue("%s%s%s".formatted(
							currentValue.getValue().substring(0, numberIndex),
							input,
							' '));
				} else {
					result = currentValue.withValue("%s%s%s".formatted(
							currentValue.getValue().substring(0, numberIndex),
							input,
							currentValue.getValue().substring(numberIndex + 1)));
				}
			}
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class State {

		List<Objects> actions;

		String anatomicalElement;

		List<ModuleParameter> externalParameters;

		List<ModuleParameter> internalParameters;

		@CheckForNull
		List<String> selectedCellPath;
	}
}
