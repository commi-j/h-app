package tk.labyrinth.happ.application.domain.correctionmoduleparametercell;

import com.vaadin.flow.component.Component;
import com.vaadin.flow.component.html.Label;
import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.domain.widget.CellWidget;
import tk.labyrinth.happ.application.vaadin.HtmlTable;
import tk.labyrinth.happ.application.vaadin.HtmlTableRow;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BorderCollapse;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.AlignItems;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.CssHorizontalLayout;
import tk.labyrinth.pandora.misc4j.lib.vaadin.layout.cssflexbox.JustifyContent;
import tk.labyrinth.pandora.ui.style.PandoraStyles;

import java.util.function.Consumer;
import java.util.function.Function;

public class CorrectionModuleParameterCellEditor {

	public static Component render(Properties properties) {
		CssHorizontalLayout result = new CssHorizontalLayout();
		{
			result.addClassNames(PandoraStyles.LAYOUT);
			result.setJustifyContent(JustifyContent.SPACE_AROUND);
			result.setAlignItems(AlignItems.BASELINE);
		}
		{
			{
				result.add(new HtmlTable(
						new HtmlTableRow(List
								.of("-", "+", " ")
								.map(value -> CellWidget.render(CellWidget.Properties.builder()
										.mode(properties.getSignEnabled()
												? CellWidget.Mode.NORMAL
												: CellWidget.Mode.DISABLED)
										.onClick(() -> properties.getOnInput().accept(List.of("sign", value)))
										.value(value)
										.build()))))
						.setBorderCollapse(BorderCollapse.COLLAPSE)
						.setBorderSpacing("0px"));
			}
			{
				result.add(new HtmlTable(
						new HtmlTableRow(List.of(
										List.of(","),
										List.rangeClosed(0, 4).map(Object::toString),
										List.rangeClosed(5, 9).map(Object::toString),
										List.of(" "))
								.flatMap(Function.identity())
								.map(value -> CellWidget.render(CellWidget.Properties.builder()
										.mode(properties.getNumberEnabled()
												? CellWidget.Mode.NORMAL
												: CellWidget.Mode.DISABLED)
										.onClick(() -> properties.getOnInput().accept(List.of("number", value)))
										.value(value)
										.build()))))
						.setBorderCollapse(BorderCollapse.COLLAPSE)
						.setBorderSpacing("0px"));
			}
			{
				// CONTROL
				result.add(new Label("CONTROL"));
			}
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Boolean controlEnabled;

		@NonNull
		Boolean numberEnabled;

		@NonNull
		Consumer<List<String>> onInput;

		@NonNull
		Boolean signEnabled;
	}
}
