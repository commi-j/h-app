package tk.labyrinth.happ.application.vaadin;

import com.vaadin.flow.component.HasElement;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BorderCollapse;

public interface CssTable<T extends CssTable<T>> extends HasElement {

	@SuppressWarnings("unchecked")
	default T setBorderCollapse(BorderCollapse borderCollapse) {
		getElement().getStyle().set("border-collapse", borderCollapse.name().toLowerCase());
		return (T) this;
	}

	@SuppressWarnings("unchecked")
	default T setBorderSpacing(String borderSpacing) {
		getElement().getStyle().set("border-spacing", borderSpacing);
		return (T) this;
	}
}
