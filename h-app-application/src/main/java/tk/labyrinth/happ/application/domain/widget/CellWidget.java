package tk.labyrinth.happ.application.domain.widget;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.vaadin.HtmlTableData;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.Cursor;
import tk.labyrinth.pandora.misc4j.lib.vaadin.dom.StyleUtils;

public class CellWidget {

	public enum Mode {
		DISABLED,
		HIGHLIGHTED,
		NORMAL,
	}

	public static HtmlTableData render(Properties properties) {
		HtmlTableData result = new HtmlTableData();
		{
			StyleUtils.setBackgroundColor(result, properties.getMode() == Mode.HIGHLIGHTED ? "lightblue" : null);
			StyleUtils.setBorder(result, "1px solid lightgrey");
			StyleUtils.setColor(result, properties.getMode() == Mode.DISABLED ? "lightgrey" : null);
			StyleUtils.setCssProperty(result, Cursor.DEFAULT);
			result.setHeight("1.5em");
			StyleUtils.setTextAlign(result, "center");
			result.setWidth("1.5em");
		}
		{
			if (properties.getMode() != Mode.DISABLED) {
				result.addClickListener(event -> properties.getOnClick().run());
			}
			result.setText(properties.getValue());
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Mode mode;

		@NonNull
		Runnable onClick;

		@NonNull
		String value;
	}
}
