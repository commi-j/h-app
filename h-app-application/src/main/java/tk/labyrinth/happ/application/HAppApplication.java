package tk.labyrinth.happ.application;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;

@Slf4j
@SpringBootApplication
public class HAppApplication {

	@Value("${application.build.timestamp}")
	private String applicationBuildTimestamp;

	@PostConstruct
	private void postConstruct() {
		logger.info("application.build.timestamp = {}", applicationBuildTimestamp);
	}

	public static void main(String... args) {
		{
			// There was a problem accessing KafkaConsumer package private constructor from
			// org.springframework.boot.devtools.restart.classloader.RestartClassLoader.
			// https://stackoverflow.com/questions/57750294/class-loader-error-unnamed-module-of-loader-org-springframework-boot-devtools
			// https://docs.spring.io/spring-boot/docs/current/reference/htmlsingle/#using.devtools.restart.disable
			//
			System.setProperty("spring.devtools.restart.enabled", "false");
		}
		{
			SpringApplication.run(HAppApplication.class, args);
		}
	}
}
