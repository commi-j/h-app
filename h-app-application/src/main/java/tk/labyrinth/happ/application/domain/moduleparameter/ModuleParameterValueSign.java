package tk.labyrinth.happ.application.domain.moduleparameter;

public enum ModuleParameterValueSign {
	MINUS,
	NOT_APPLICABLE,
	PLUS,
	UNDEFINED;

	public static ModuleParameterValueSign parse(Character character) {
		return switch (character) {
			case '-' -> MINUS;
			case '+' -> PLUS;
			case ' ' -> UNDEFINED;
			default -> throw new IllegalArgumentException();
		};
	}
}
