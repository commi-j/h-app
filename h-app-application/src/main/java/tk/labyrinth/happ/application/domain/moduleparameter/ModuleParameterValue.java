package tk.labyrinth.happ.application.domain.moduleparameter;

import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;

@Builder(toBuilder = true)
@Value
@With
public class ModuleParameterValue {

	@NonNull
	ModuleParameterValueSign sign;

	@NonNull
	String value;

	public static ModuleParameterValue empty(boolean hasSign) {
		return ModuleParameterValue.builder()
				.sign(hasSign ? ModuleParameterValueSign.UNDEFINED : ModuleParameterValueSign.NOT_APPLICABLE)
				.value(" ")
				.build();
	}
}
