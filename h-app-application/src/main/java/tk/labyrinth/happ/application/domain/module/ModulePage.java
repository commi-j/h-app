package tk.labyrinth.happ.application.domain.module;

import com.vaadin.flow.component.html.Label;
import com.vaadin.flow.component.splitlayout.SplitLayout;
import com.vaadin.flow.router.Route;
import tk.labyrinth.happ.application.domain.functionalmodule.FunctionalModuleView;

import javax.annotation.PostConstruct;

@Route("module")
public class ModulePage extends SplitLayout {

	@PostConstruct
	private void postConstruct() {
		{
			setSizeFull();
			setSplitterPosition(50);
		}
		{
			addToPrimary(new FunctionalModuleView());
		}
		{
			SplitLayout s = new SplitLayout();
			{
				s.setOrientation(Orientation.VERTICAL);
				s.setSplitterPosition(50);
			}
			{
				s.addToPrimary(new Label("S0"));
			}
			{
//				s.addToSecondary(new InputBoard());
			}
			addToSecondary(s);
		}
	}
//	@PostConstruct
//	private void postConstruct() {
//		{
//			setGridTemplateAreas(
//					"module options",
//					"module value",
//					"module table",
//					"numbers table");
//			setGridTemplateColumns("1fr auto");
//			setGridTemplateRows("auto auto 1fr auto");
//			setHeightFull();
//		}
//		{
//			{
//				Label moduleLabel = new Label("MODULE");
//				StyleUtils.setBackgroundColor(moduleLabel, "red");
//				add(moduleLabel, "module");
//			}
//			{
//				Label optionsLabel = new Label("OPTIONS");
//				StyleUtils.setBackgroundColor(optionsLabel, "green");
//				add(optionsLabel, "options");
//			}
//			{
//				Label valueLabel = new Label("VALUE");
//				StyleUtils.setBackgroundColor(valueLabel, "pink");
//				add(valueLabel, "value");
//			}
//			{
//				Label tableLabel = new Label("TABLE");
//				StyleUtils.setBackgroundColor(tableLabel, "blue");
//				add(tableLabel, "table");
//			}
//			{
//				Label numbersLabel = new Label("NUMBERS");
//				StyleUtils.setBackgroundColor(numbersLabel, "lightblue");
//				add(numbersLabel, "numbers");
//			}
//		}
//	}
}
