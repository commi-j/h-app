package tk.labyrinth.happ.application.domain.moduleparameter;

import io.vavr.collection.List;
import lombok.Builder;
import lombok.NonNull;
import lombok.Value;
import lombok.With;
import tk.labyrinth.happ.application.domain.widget.CellWidget;
import tk.labyrinth.happ.application.vaadin.HtmlTable;
import tk.labyrinth.happ.application.vaadin.HtmlTableRow;
import tk.labyrinth.pandora.misc4j.exception.UnreachableStateException;
import tk.labyrinth.pandora.misc4j.lib.vaadin.css.BorderCollapse;

import javax.annotation.CheckForNull;
import java.util.Objects;
import java.util.function.Consumer;

public class ModuleParameterValueView {

	public static HtmlTable render(Properties properties) {
		HtmlTable result;
		{
			HtmlTableRow row = new HtmlTableRow();
			//
			if (properties.getValue().getSign() != ModuleParameterValueSign.NOT_APPLICABLE) {
				Character character = switch (properties.getValue().getSign()) {
					case MINUS -> '-';
					case PLUS -> '+';
					case UNDEFINED -> ' ';
					case NOT_APPLICABLE -> throw new UnreachableStateException();
				};
				row.add(CellWidget.render(CellWidget.Properties.builder()
						.mode(Objects.equals(properties.getSelectedCellPath(), "sign")
								? CellWidget.Mode.HIGHLIGHTED
								: CellWidget.Mode.NORMAL)
						.onClick(() -> properties.getOnCellSelect().accept("sign"))
						.value(character.toString())
						.build()));
			}
			List.ofAll(properties.getValue().getValue().toCharArray())
					.zipWithIndex()
					.forEach(tuple -> {
						row.add(CellWidget.render(CellWidget.Properties.builder()
								.mode(Objects.equals(properties.getSelectedCellPath(), tuple._2().toString())
										? CellWidget.Mode.HIGHLIGHTED
										: CellWidget.Mode.NORMAL)
								.onClick(() -> properties.getOnCellSelect().accept(tuple._2().toString()))
								.value(tuple._1().toString())
								.build()));
					});
			//
			result = new HtmlTable(row)
					.setBorderCollapse(BorderCollapse.COLLAPSE)
					.setBorderSpacing("0px");
		}
		return result;
	}

	@Builder(toBuilder = true)
	@Value
	@With
	public static class Properties {

		@NonNull
		Consumer<String> onCellSelect;

		@CheckForNull
		String selectedCellPath;

		@NonNull
		ModuleParameterValue value;
	}
}
